<!DOCTYPE html>
<html>
<head>
<title>Tugas OOP</title>
</head>
<body>
    <h1>HARI 9 - OOP PHP</h1>
    <?php
        require_once('animal.php');
        require_once('ape.php');
        require_once('frog.php');

        //untuk class animal
        $sheep = new Animal("shaun");
        echo "Name : " . $sheep->name . "<br>";
        echo "Legs : " . $sheep->legs . "<br>";
        echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

        //untuk class Frog
        $kodok = new Frog("buduk");
        echo "Name : " . $kodok->name . "<br>";
        echo "Legs : " . $kodok->legs . "<br>";
        echo "Cold Blooded : " . $kodok->cold_blooded . "<br>" ;
        echo $kodok->jump() . "<br>";

        //untuk class Ape
        $sungokong = new Ape("kera sakti"); 
        echo "Name : " . $sungokong->name  . "<br>";
        echo "Legs : " . $sungokong->legs . "<br>";
        echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
        echo $sungokong->yell(); 
    ?>

</body>
</html>

