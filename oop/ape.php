<?php

require_once('animal.php');

//karena default legs di class animal adalah 4, maka ape perlu inisiasi ulang agar legsnya 2 dan memiliki sifat tambahan yaitu yell
class Ape extends Animal{
    public $legs = 2;
    public function yell() {
        echo "Yell : Auoooooo <br><br>";
    }
}