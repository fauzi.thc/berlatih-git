<?php

class Animal
{
    //name dikosongkan dulu karena setiap hewan punya nama beda, sedangkan legs dan cols diisi karena ada hewan yg punya kesamaan
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";


    public function __construct($get_name){
        $this->name = $get_name;
    }
}