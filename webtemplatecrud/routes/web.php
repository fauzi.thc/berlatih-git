<?php



Route::get('/', function () {
    return view('halaman.table');
});

Route::get('/data-tables', function(){
    return view('halaman.data-tables');
});
Route::get('/table', function(){
    return view('halaman.table');
});

//CRUD cast
//menampilkan cast
Route::get('/cast', 'CastController@index');
//menampilkan form tambah cast
Route::get('/cast/create', 'CastController@create');
//save data cast yang ditambah
Route::post('/cast', 'CastController@store');
//menampilkan detail cast
Route::get('/cast/{cast_id}', 'CastController@show');
//mengarah ke form edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//update data ke database
Route::put('/cast/{cast_id}', 'CastController@update');
//hapus data
Route::delete('/cast/{cast_id}', 'CastController@destroy');
