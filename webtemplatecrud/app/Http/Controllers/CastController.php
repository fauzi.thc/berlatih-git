<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{

    public function index(){
        $casts = DB::table('casts')->get();
 
        return view('cast.index', compact('casts'));
    }

    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|int',
                'bio' => 'required',
            ],

            [
                'nama.required' => 'Nama tidak boleh kosong',
                'umur.required'  => 'Umur tidak boleh kosong',
                'bio.required'  => 'Bio tidak boleh kosong.',
            ]
        );

        DB::table('casts')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast');

    }

    public function show($id){
        $cast = DB::table('casts')->where('id', $id)->first();

        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('casts')->where('id', $id)->first();

        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|int',
                'bio' => 'required',
            ],

            [
                'nama.required' => 'Nama tidak boleh kosong',
                'umur.required'  => 'Umur tidak boleh kosong',
                'bio.required'  => 'Bio tidak boleh kosong.',
            ]
        );

        DB::table('casts')->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],   
                ]
            );
        
        return redirect('/cast');

    }

    public function destroy($id){
        DB::table('casts')->where('id', '=', $id)->delete();

        return redirect('/cast');

    }

       
    
}
