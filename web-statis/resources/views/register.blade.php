<!DOCTYPE html>
<html>
<head>
<title>Form</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
  
    <form action="/welcome" method="POST">
        @csrf
        <label>First Name : </label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name : </label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender </label><br><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value ="indonesia">Indonesia</option>
            <option value ="Malaysia">Malaysia</option>
            <option value ="Singapura">Singapura</option>
            <option value ="Amerika">Amerika</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" rows="10" cols="30"</textarea></textarea><br><br>

        <input type="submit" value="Sign Up">
        <br>
        <br>
    </form>
</body>
</html>